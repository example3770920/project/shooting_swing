package main;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import subClass.Boom;
import subClass.Item;
import subClass.Missile;
import subClass.Monster;

public class ShootingMain extends JFrame implements KeyListener, Runnable {

	private int f_width, f_height; // 프레임의 크기
	private int user_x, user_y; // 사용자 좌표
	private int user_shield;// 사용자 방어력
	private int backGround_y1, backGround_y2;// 배경 좌표
	private int cnt;// 게임 흐름 카운터
	private int missile_power;// 미사일 공격력
	private int score;// 게임점수
	private int monster_shield;// 몬스터 방어력
	private int monster_speed;// 몬스터 속도

	private boolean user_Left, user_Right, game_stop, game_start, game_close; // 사용자 조작키
	private boolean game;

	private Image user_img;// 케릭터 이미지
	private Image[] backGround_img;// 배경화면 이미지 무한스크롤 하기위해 2개
	private Image monster_img;// 적 이미지
	private Image missile_img;// 미사일 이미지
	private Image[] item_img;// 아이템 이미지 1.점수, 2.미사일 수 증가,3.5초간 무적
	private Image[] boom_img;// 폭파 이미지
	private Image score_img;// 점수 이미지

	private Image buffImage;// 더블 버퍼용
	private Graphics buffg;// 더블 버퍼용

	private Toolkit tk = Toolkit.getDefaultToolkit();
	private Thread th;
	private Random rand = new Random();

	private Missile missile;
	private Monster monster;
	private Item item;
	private Boom boom;

	private ArrayList<Missile> missileList = new ArrayList<Missile>();
	private ArrayList<Monster> monsterList = new ArrayList<Monster>();
	private ArrayList<Item> itemList = new ArrayList<Item>();
	private ArrayList<Boom> boomList = new ArrayList<Boom>();


	public ShootingMain() {
		super("Shooting_Game");
		this.setFrame();
		Dimension screen = tk.getScreenSize();
		int f_x = (int) (screen.getWidth() / 2 - f_width / 2);
		int f_y = (int) (screen.getHeight() / 2 - f_height / 2);
		this.setBounds(f_x, f_y, f_width, f_height);// 프레임 띄울 위치와 크기
		this.setResizable(false);// 프레임 크기 변경 x
		this.setVisible(true);// 프레임 눈에보이게
		this.start();
		this.setForeground(new Color(255,255,255));
	}

	public void setFrame() {
		f_height = 600;
		f_width = 400;
		user_x = f_width / 2;
		user_y = 500;
		backGround_y1 = 0;
		backGround_y2 = -800;
		missile_power = 1;

		game = true;

		user_img = new ImageIcon("./img/user.png").getImage();
		backGround_img = new Image[2];
		backGround_img[0] = new ImageIcon("./img/ground.png").getImage();
		backGround_img[1] = new ImageIcon("./img/ground.png").getImage();
		// monster_img=new ImageIcon("./img/monster.png").getImage();
		missile_img = new ImageIcon("./img/missile.png").getImage();
		item_img = new Image[3];
		for (int i = 0; i < item_img.length; i++) {
			item_img[i] = new ImageIcon("./img/item" + (i + 1) + ".png")
					.getImage();
		}
		boom_img = new Image[2];
		boom_img[0] = new ImageIcon("./img/boom1.png").getImage();
		boom_img[1] = new ImageIcon("./img/boom2.png").getImage();
		score_img=new ImageIcon("./img/score.png").getImage();
	}

	public void start() {
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.addKeyListener(this);
		th = new Thread(this);
		th.start();

	}

	public static void main(String[] args) {
		new ShootingMain();
	}

	@Override
	public void run() {
		try {
			while (game) {
				gameProcess();
				userProcess();
				monsterProcess();
				missileProcess();
				itemProcess();
				boomProcess();
				repaint();
				Thread.sleep(10);
				cnt++;//0.01초
			}
		} catch (InterruptedException e) {

			e.printStackTrace();
		}

	}

	public void paint(Graphics g) {
		buffImage = createImage(f_width, f_height);
		buffg = buffImage.getGraphics();

		update(g);
	}

	public void update(Graphics g) {

		drawBackGround();// 배경 그리기
		drawUser();// 사용자 그리기
		drawMonster();// 몬스터 그리기
		drawMissile();// 미사일 그리기
		drawItem();// 아이템 그리기
		drawBoom();// 폭파 그리기
		g.drawImage(buffImage, 0, 0, this);
	}

	public void gameProcess() {

		if (cnt >= 7500) {
			user_y -= 10;
			if (user_y <= 0) {
				game = false;
				int check = JOptionPane.showConfirmDialog(this,
						"게임에 승리하였습니다.\n" + "다시 시작 하시겠습니까?\n점수---->" + score,
						"Shooting_Game", JOptionPane.CANCEL_OPTION);
				if (check == 0) {
					reset();
				} else if (check == 2) {
					System.exit(0);
				}


			}
		}
	}

	public void userProcess() {
		if (user_shield == cnt) {
			user_shield = 0;
			user_img = new ImageIcon("./img/user.png").getImage();
		}
		if (game_stop) {

			game = false;// 쓰레드 제어
			int check = JOptionPane.showConfirmDialog(this, "게임을 종료하시겠습니까?",
					"Shooting_Game", JOptionPane.CANCEL_OPTION);

			if (check == 0) {
				// 게임 종료
				System.exit(0);
			} else  {
				// 다시 시작
				game_stop = false;
				game = true;
			}
		}
		if (user_Left) {
			if (user_x >= 5) {// 화면에 넘어가지 못하게
				user_x -= 5;
			}
		}
		if (user_Right) {
			if (user_x <= 350) {// 화면에 넘어가지 못하게
				user_x += 5;
			}
		}
	}

	public void monsterProcess() {
		if (cnt >= 0 && cnt <= 2000) {
			monster_img = new ImageIcon("./img/monster1.png").getImage();
			monster_shield = 3;
			monster_speed = 3;
		} else if (cnt >= 2000 && cnt <= 4000) {
			monster_img = new ImageIcon("./img/monster2.png").getImage();
			monster_shield = 5;
			monster_speed = 4;
		} else if (cnt >= 4000 && cnt <= 6000) {
			monster_img = new ImageIcon("./img/monster3.png").getImage();
			monster_shield = 7;
			monster_speed = 5;
		}
		for (int i = 0; i < monsterList.size(); i++) {
			monster = monsterList.get(i);
			monster.move();
			if (monster.getY() >= 800) {// 몬스터 화면 밖으로 나가면 삭제
				monsterList.remove(i);
			}
			if (check(user_x, user_y, user_img, monster.getX(), monster.getY(),
					monster_img)) {
				if (user_shield == 0) {
					boom = new Boom(monster.getX(), monster.getY(), cnt + 50);
					boomList.add(boom);
					boom = new Boom(user_x, user_y, cnt + 50);
					boomList.add(boom);
					monsterList.remove(i);// 몬스터가 사용자와 부디치면 삭제
					user_shield = 1;

				} else if (user_shield != 0) {
					boom = new Boom(monster.getX(), monster.getY(), cnt + 50);
					boomList.add(boom);
					int rn = rand.nextInt(10) + 1;
// System.out.println("아이템 번호--->" + rn);
					switch (rn) {
						case 2:

							item = new Item(monster.getX(), monster.getY() - 30, 4,
									rn);
							itemList.add(item);
							break;
						case 3:
							item = new Item(monster.getX(), monster.getY() - 30, 4,
									rn);
							itemList.add(item);

							break;
						default:
							item = new Item(monster.getX(), monster.getY() - 30, 4,
									1);
							itemList.add(item);

							break;
					}
					monsterList.remove(i);// 몬스터가 사용자와 부디치면 삭제

				}

			}
		}
		int x = 0;
		if (cnt % 300 == 0&&cnt<=6500) {
			for (int i = 0; i < 5; i++) {
				monster = new Monster(x, -10, monster_speed, monster_shield);
				monsterList.add(monster);
				x += 80;
			}
		}
	}

	public void missileProcess() {
		for (int i = 0; i < missileList.size(); i++) {
			missile = missileList.get(i);
			missile.move();
			if (missile.getY() <= 0) {
				missileList.remove(i);
			}
			for (int l = 0; l < monsterList.size(); l++) {
				monster = monsterList.get(l);
				if (check(missile.getX(), missile.getY(), missile_img,
						monster.getX(), monster.getY(), monster_img)) {

					if (missileList.size() != 0) {
//System.out.println("삭제되는 i------>" + i);
						try {
							missileList.remove(i);
						} catch (IndexOutOfBoundsException e) {
							e.printStackTrace();
							break;
						}
					}

					monster.setHit(missile_power);
					if (monster.getShield() <= 0) {
						boom = new Boom(monster.getX(), monster.getY(),
								cnt + 50);
						boomList.add(boom);// 폭파 처리
						// 몬스터가 죽었을때 랜덤으로 아이템 생성
						int rn = rand.nextInt(10) + 1;
// System.out.println("아이템 번호--->" + rn);
						switch (rn) {
							case 2:

								item = new Item(monster.getX(),
										monster.getY() - 30, 4, rn);
								itemList.add(item);
								break;
							case 3:
								item = new Item(monster.getX(),
										monster.getY() - 30, 4, rn);
								itemList.add(item);

								break;
							default:
								item = new Item(monster.getX(),
										monster.getY() - 30, 4, 1);
								itemList.add(item);

								break;
						}
						monsterList.remove(l);
					}

				}

			}
		}
		if (cnt % 20 == 0 && cnt <= 7000) {

			missile = new Missile(user_x + 12, user_y - 10, 5, missile_power);
			missileList.add(missile);

		}
	}

	public void itemProcess() {
		for (int i = 0; i < itemList.size(); i++) {
			item = itemList.get(i);
			item.move();
			if (item.getY() >= 800) {
				itemList.remove(i);// 화면 밖으로 나가면 삭제
			}
			if (check(user_x, user_y, user_img, item.getX(), item.getY(),
					item_img[item.getType() - 1])) {
				switch (item.getType()) {
					case 2:// 2번 아이템 획득시 미사일 수 증가
						if (missile_power <= 2) {
							missile_power++;
// System.out.println("미사일 수--->" + missile_power);
						}
						break;
					case 3:
						user_img = new ImageIcon("./img/user2.png").getImage();
						user_shield = cnt + 800;// 8초간 무적
						break;

					default:
						score += 10;
						break;
				}
				itemList.remove(i);
			}
		}
	}

	public void boomProcess() {
		for (int i = 0; i < boomList.size(); i++) {
			boom = boomList.get(i);

			if (boom.getTime() <= cnt) {
				// 폭파 시간 끝나면 폭파 이미지 제거
				boomList.remove(i);
				if (user_shield == 1) {
					game = false;// 게임패배
					int check = JOptionPane.showConfirmDialog(this, "점수--->"
									+ score + "\n 게임을 다시 시작 하시겠습니까?", "Shooting_Game",
							JOptionPane.CANCEL_OPTION);
					if (check == 0) {
						// 다시 시작
						reset();
						game_stop = false;
						game = true;
					} else if (check == 2) {
						// 게임 종료
						System.exit(0);

					}
				}
			}
		}
	}

	public void drawBackGround() {

		buffg.clearRect(0, 0, f_width, f_height);
		buffg.drawImage(backGround_img[0], 0, backGround_y1, this);
		buffg.drawImage(backGround_img[1], 0, backGround_y2, this);
		buffg.drawImage(score_img, 10, 30, this);
		buffg.drawString(""+score , 130, 50);



		backGround_y1 += 2;
		backGround_y2 += 2;

		if (backGround_y1 >= 800) {
			backGround_y1 = 0;
		}
		if (backGround_y2 >= 0) {
			backGround_y2 = -800;
		}

	}

	public void drawUser() {
		buffg.drawImage(user_img, user_x, user_y, this);
	}

	public void drawMonster() {

		for (int i = 0; i < monsterList.size(); i++) {
			monster = monsterList.get(i);
			buffg.drawImage(monster_img, monster.getX(), monster.getY(), this);
		}
	}

	public void drawMissile() {

		for (int i = 0; i < missileList.size(); i++) {
			missile = missileList.get(i);
			if (missile_power == 1) {
				buffg.drawImage(missile_img, missile.getX(), missile.getY(),
						this);
			} else if (missile_power == 2) {
				buffg.drawImage(missile_img, missile.getX(), missile.getY(),
						this);
				buffg.drawImage(missile_img, missile.getX() - 10,
						missile.getY(), this);
			} else if (missile_power == 3) {

				buffg.drawImage(missile_img, missile.getX() - 12,
						missile.getY(), this);
				buffg.drawImage(missile_img, missile.getX() + 12,
						missile.getY(), this);
				buffg.drawImage(missile_img, missile.getX(),
						missile.getY() - 5, this);

			}
		}

	}

	public void drawItem() {
		for (int i = 0; i < itemList.size(); i++) {
			item = itemList.get(i);
			buffg.drawImage(item_img[item.getType() - 1], item.getX(),
					item.getY(), this);
		}
	}

	public void drawBoom() {
		for (int i = 0; i < boomList.size(); i++) {
			boom = boomList.get(i);
			if (boom.getTime() - 25 >= cnt) {
				buffg.drawImage(boom_img[0], boom.getX(), boom.getY(), this);
			} else if (boom.getTime() >= cnt) {
				buffg.drawImage(boom_img[1], boom.getX(), boom.getY(), this);
			}

		}
	}

	@Override
	public void keyPressed(KeyEvent e) {
		int key = e.getKeyCode();

		switch (key) {
			case KeyEvent.VK_LEFT:
				user_Left = true;
				break;
			case KeyEvent.VK_RIGHT:
				user_Right = true;
				break;
			case KeyEvent.VK_ESCAPE:
				game_stop = true;
				break;

			default:
				break;
		}

	}

	@Override
	public void keyReleased(KeyEvent e) {

		int key = e.getKeyCode();

		switch (key) {
			case KeyEvent.VK_LEFT:
				user_Left = false;
				break;
			case KeyEvent.VK_RIGHT:
				user_Right = false;
				break;
			case KeyEvent.VK_ESCAPE:
				game_stop = false;
				break;

			default:
				break;
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {

	}

	public boolean check(int x1, int y1, Image img1, int x2, int y2, Image img2) {
		boolean bool;
		if (Math.abs((x1 + img1.getWidth(null) / 2)
				- (x2 + img2.getWidth(null) / 2)) < (img2.getWidth(null) / 2 + img1
				.getWidth(null) / 2)
				&& Math.abs((y1 + img1.getHeight(null) / 2)
				- (y2 + img2.getHeight(null) / 2)) < (img2
				.getHeight(null) / 2 + img1.getHeight(null) / 2)) {
			// 이미지 넓이, 높이값을 바로 받아 계산합니다.
			bool = true;
		} else {
			bool = false;
		}
		return bool;
	}

	public void reset() {
		user_x = f_width / 2;
		user_y=500;
		user_shield = 0;// 사용자 방어력
		cnt = 0;// 게임 흐름 카운터
		missile_power = 1;// 미사일 공격력
		score = 0;// 게임점수
		monster_shield = 3;// 몬스터 방어력
		monster_speed = 3;// 몬스터 속도
		user_Left = false;
		user_Right = false;
		game_stop = false;
		game_start = false;
		game_close = false;
		game = true;
		if (missileList.size() != 0) {
			missileList.clear();
		}
		if (monsterList.size() != 0) {
			monsterList.clear();
		}
		if (itemList.size() != 0) {
			itemList.clear();
		}
		if (boomList.size() != 0) {
			boomList.clear();
		}
	}
}
