package subClass;

public class Monster {
	private int x,y;
	private int speed;
	private int shield;
	public Monster(int x, int y, int speed,int shield){
		this.x=x;
		this.y=y;
		this.speed=speed;
		this.shield=shield;
	}
	public void move(){
		y+=speed;
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public int getSpeed() {
		return speed;
	}
	public void setSpeed(int speed) {
		this.speed = speed;
	}
	public int getShield() {
		return shield;
	}
	public void setShield(int shield) {
		this.shield = shield;
	}
	public void setHit(int power){
		shield-=power;
	}
	
	
}
