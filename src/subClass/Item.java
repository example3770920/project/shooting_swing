package subClass;

import java.util.Random;

public class Item {
	private int x,y;
	private int speed;
	private int type;
	private int cnt;
	private Random rand=new Random();
	private int location;// 아이템 떨어지는 방향
	public Item(int x, int y, int speed, int type){
		this.x=x;
		this.y=y;
		this.speed=speed;
		this.type=type;
		location=rand.nextInt();
	}
	public void move(){
		cnt+=speed;
		if(cnt<=100){
			y-=speed-2;
			if(location%2==0){
				x+=speed-3;
			}else{
				x-=speed-3;
			}
		}else{
			if(cnt<=200){
				y+=speed-1;
			}else{

				y+=speed;
			}
		}
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public int getSpeed() {
		return speed;
	}
	public void setSpeed(int speed) {
		this.speed = speed;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}

}
