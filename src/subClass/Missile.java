package subClass;

public class Missile {
	private int x,y;
	private int speed;
	private int power;
	public Missile(int x, int y, int speed,int power){
		this.x=x;
		this.y=y;
		this.speed=speed;
		this.power=power;
	}
	public void move(){
		y-=speed;
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public int getSpeed() {
		return speed;
	}
	public void setSpeed(int speed) {
		this.speed = speed;
	}
	public int getPower() {
		return power;
	}
	public void setPower(int power) {
		this.power = power;
	}
	
	
}
